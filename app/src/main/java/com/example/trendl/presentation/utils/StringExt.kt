package com.example.trendl.presentation.utils

fun String.splitUrl(): Pair<String, String> {
    val baseUrl = "${substringBefore('?')}/"
    val urlToExecute = "?${substringAfter('?')}"
    return baseUrl to urlToExecute
}