package com.example.trendl.presentation.utils.connectivity

interface IConnectivityManager {
    val isOnline: Boolean
}