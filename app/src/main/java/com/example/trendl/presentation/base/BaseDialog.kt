package com.example.trendl.presentation.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController

abstract class BaseDialog<B : ViewDataBinding> : DialogFragment() {

    protected lateinit var navController: NavController
    protected lateinit var binding: B

    @LayoutRes
    abstract fun getLayoutResId(): Int

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        navController = findNavController()
        val view = View.inflate(requireContext(), getLayoutResId(), null)
        return Dialog(requireActivity(), android.R.style.Theme_NoTitleBar_Fullscreen).apply {
            setContentView(view)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutResId(), container, false)
        return binding.root
    }
}