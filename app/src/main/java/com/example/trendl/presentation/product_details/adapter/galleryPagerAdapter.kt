package com.example.trendl.presentation.product_details.adapter

import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.trendl.databinding.AdapterGalleryBinding
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

fun galleryPagerAdapterDelegate() =
    adapterDelegateViewBinding<String, String, AdapterGalleryBinding>({ layoutInflater, root ->
        AdapterGalleryBinding.inflate(layoutInflater, root, false)
    }) {
        bind {
            Glide.with(itemView.context)
                .load(item)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(binding.image)
        }
    }