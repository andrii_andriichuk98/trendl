package com.example.trendl.presentation.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.example.trendl.R
import com.example.trendl.data.uimodel.*
import com.example.trendl.databinding.ItemBannerSingleBinding
import com.example.trendl.databinding.ItemWidgetListBinding
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

internal fun createPicturesAdapter(itemClickedListener: (product: DisplayUiModel) -> Unit): ListDelegationAdapter<List<BaseWidgetUIModel>> {
    return ListDelegationAdapter(
        singleBannerAdapterDelegate(),
        productSliderAdapterDelegate(itemClickedListener),
        sliderBannerAdapterDelegate(itemClickedListener),
        productListingAdapterDelegate(itemClickedListener),
        carouselBannerAdapterDelegate(itemClickedListener)
    )
}

private fun singleBannerAdapterDelegate() =
    adapterDelegateViewBinding<SingleBannerUIModel, BaseWidgetUIModel, ItemBannerSingleBinding>(
        { layoutInflater, root -> ItemBannerSingleBinding.inflate(layoutInflater, root, false) }
    ) {
        bind {
            item.contents.firstOrNull()?.let { image ->
                Glide.with(itemView.context)
                    .load(image.imageUrl)
                    .into(binding.imageBanner)
            }
        }
    }

private fun carouselBannerAdapterDelegate(itemClickedListener: (product: DisplayUiModel) -> Unit) =
    adapterDelegateOtherListViewBinding<CarouselBannerUIModel, ItemWidgetListBinding>(
        carouselBannerInnerAdapterDelegate(itemClickedListener),
        viewBinding = { layoutInflater, root ->
            ItemWidgetListBinding.inflate(layoutInflater, root, false)
        }
    )

private fun sliderBannerAdapterDelegate(itemClickedListener: (product: DisplayUiModel) -> Unit) =
    adapterDelegateOtherListViewBinding<SliderBannerUIModel, ItemWidgetListBinding>(
        sliderBannerInnerAdapterDelegate(itemClickedListener),
        viewBinding = { layoutInflater, root ->
            ItemWidgetListBinding.inflate(layoutInflater, root, false)
        }
    )

private fun productSliderAdapterDelegate(itemClickedListener: (product: DisplayUiModel) -> Unit) =
    adapterDelegateOtherListViewBinding<ProductSliderUIModel, ItemWidgetListBinding>(
        productSliderInnerAdapterDelegate(itemClickedListener),
        viewBinding = { layoutInflater, root ->
            ItemWidgetListBinding.inflate(layoutInflater, root, false)
        }
    )

private fun productListingAdapterDelegate(itemClickedListener: (product: DisplayUiModel) -> Unit) =
    adapterDelegateOtherListViewBinding<ProductListUIModel, ItemWidgetListBinding>(
        productListingInnerAdapterDelegate(itemClickedListener),
        layoutManagerType = LayoutManagerType.GRID,
        viewBinding = { layoutInflater, root ->
            ItemWidgetListBinding.inflate(layoutInflater, root, false)
        }
    )

private inline fun <reified T : BaseWidgetUIModel, V : ViewBinding> adapterDelegateOtherListViewBinding(
    adapterDelegate: AdapterDelegate<List<DisplayUiModel>>,
    layoutManagerType: LayoutManagerType = LayoutManagerType.LINEAR,
    noinline viewBinding: (layoutInflater: LayoutInflater, parent: ViewGroup) -> V
): AdapterDelegate<List<BaseWidgetUIModel>> {
    return adapterDelegateViewBinding(
        viewBinding,
        on = { item, _, _ -> item is T },
        layoutInflater = { parent -> LayoutInflater.from(parent.context) },
        block = {
            val adapter: ListDelegationAdapter<List<DisplayUiModel>> by lazy(
                LazyThreadSafetyMode.NONE
            ) {
                ListDelegationAdapter(adapterDelegate)
            }
            bind {
                binding.root.findViewById<RecyclerView>(R.id.rvCarouselBanner)?.let { rv ->
                    val layoutManager =
                        if (layoutManagerType == LayoutManagerType.LINEAR)
                            LinearLayoutManager(itemView.context, RecyclerView.HORIZONTAL, false)
                        else GridLayoutManager(itemView.context, 2)
                    rv.layoutManager = layoutManager

                    adapter.items = (item as T).contents
                    rv.adapter = adapter
                }
            }
        }
    )
}

enum class LayoutManagerType {
    LINEAR, GRID
}
