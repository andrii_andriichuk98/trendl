package com.example.trendl.presentation.utils.connectivity

interface IConnectivityListener {
    fun onNetworkStateChanged(isOnline: Boolean)
}