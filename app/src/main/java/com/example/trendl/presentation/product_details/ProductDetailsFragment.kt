package com.example.trendl.presentation.product_details

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import com.example.trendl.R
import com.example.trendl.data.uimodel.ProductUIModel
import com.example.trendl.databinding.FragmentProductDetailsBinding
import com.example.trendl.presentation.base.BaseFragment
import com.example.trendl.presentation.product_details.adapter.galleryPagerAdapterDelegate
import com.google.android.material.tabs.TabLayoutMediator
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class ProductDetailsFragment : BaseFragment<FragmentProductDetailsBinding>() {

    private val adapter: ListDelegationAdapter<List<String>> by lazy(
        LazyThreadSafetyMode.NONE
    ) {
        ListDelegationAdapter(galleryPagerAdapterDelegate())
    }

    override fun getLayoutResId(): Int = R.layout.fragment_product_details

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parseArguments()
        setOnClickListeners()
        TabLayoutMediator(binding.imageTabs, binding.productImages) { _, _ -> }.attach()
    }

    private fun parseArguments() {
        arguments?.getParcelable<ProductUIModel>(PRODUCT_EXTRA_KEY)?.let { uiModel ->
            binding.product = uiModel
            uiModel.imageUrls?.let { images ->
                adapter.items = images
                binding.productImages.adapter = adapter
            }
        }
    }

    private fun setOnClickListeners() {
        binding.btnBack.setOnClickListener { navController.popBackStack() }
    }

    companion object {
        private const val PRODUCT_EXTRA_KEY = "productExtraKey"

        fun buildArgs(uiModel: ProductUIModel): Bundle = bundleOf(
            PRODUCT_EXTRA_KEY to uiModel
        )
    }
}