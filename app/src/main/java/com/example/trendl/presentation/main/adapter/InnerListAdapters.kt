package com.example.trendl.presentation.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.example.trendl.R
import com.example.trendl.data.uimodel.DisplayUiModel
import com.example.trendl.data.uimodel.ProductUIModel
import com.example.trendl.databinding.ItemBannerCarouselBinding
import com.example.trendl.databinding.ItemBannerSliderBinding
import com.example.trendl.databinding.ItemProductListingBinding
import com.example.trendl.databinding.ItemProductSliderBinding
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding

fun carouselBannerInnerAdapterDelegate(itemClickedListener: (product: DisplayUiModel) -> Unit) =
    adapterDelegateListViewBinding(itemClickedListener,
        viewBinding = { layoutInflater, root ->
            ItemBannerCarouselBinding.inflate(layoutInflater, root, false)
        }
    )

fun productListingInnerAdapterDelegate(itemClickedListener: (product: DisplayUiModel) -> Unit) =
    adapterDelegateListViewBinding(itemClickedListener,
        viewBinding = { layoutInflater, root ->
            ItemProductListingBinding.inflate(layoutInflater, root, false)
        }
    )

fun sliderBannerInnerAdapterDelegate(itemClickedListener: (product: DisplayUiModel) -> Unit) =
    adapterDelegateListViewBinding(itemClickedListener,
        viewBinding = { layoutInflater, root ->
            ItemBannerSliderBinding.inflate(layoutInflater, root, false)
        }
    )

fun productSliderInnerAdapterDelegate(itemClickedListener: (product: DisplayUiModel) -> Unit) =
    adapterDelegateListViewBinding(itemClickedListener,
        viewBinding = { layoutInflater, root ->
            ItemProductSliderBinding.inflate(layoutInflater, root, false)
        }
    )

private fun <V : ViewBinding> adapterDelegateListViewBinding(
    itemClickedListener: (product: DisplayUiModel) -> Unit,
    viewBinding: (layoutInflater: LayoutInflater, parent: ViewGroup) -> V
): AdapterDelegate<List<DisplayUiModel>> {
    return adapterDelegateViewBinding(
        viewBinding,
        layoutInflater = { parent -> LayoutInflater.from(parent.context) },
        block = {
            itemView.setOnClickListener {
                itemClickedListener.invoke(item)
            }
            bind {
                with(binding.root) {
                    if (item.name == null) findViewById<TextView>(R.id.name)?.isVisible = false
                    findViewById<TextView>(R.id.name)?.text = item.name
                    findViewById<ImageView>(R.id.image)?.let { imageView ->
                        Glide.with(itemView.context)
                            .load(item.imageUrl)
                            .into(imageView)
                    }
                    (item as? ProductUIModel)?.let { product ->
                        findViewById<TextView>(R.id.brandName)?.text = product.brandName
                        findViewById<RatingBar>(R.id.ratingBar)?.rating =
                            product.averageRating ?: 0f
                        findViewById<TextView>(R.id.price)?.text =
                            itemView.context.getString(
                                R.string.currency,
                                product.salePrice.toString()
                            )
                    }
                }
            }
        }
    )
}