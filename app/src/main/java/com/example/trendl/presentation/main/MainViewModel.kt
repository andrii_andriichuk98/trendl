package com.example.trendl.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.trendl.data.uimodel.BaseWidgetUIModel
import com.example.trendl.domain.WidgetsUseCase
import com.example.trendl.presentation.utils.SingleLiveEvent
import com.example.trendl.presentation.utils.connectivity.ConnectivityUtil
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val widgetsUseCase: WidgetsUseCase,
    private val connectivityUtil: ConnectivityUtil
) : ViewModel() {

    private val _productData = MutableLiveData<MutableList<BaseWidgetUIModel>>()
    val productData: LiveData<MutableList<BaseWidgetUIModel>> = _productData

    private val _noInternet = SingleLiveEvent<Boolean>()
    val noInternet: SingleLiveEvent<Boolean> = _noInternet

    private val _errorMessage = MutableLiveData<String>()
    val errorMessage: LiveData<String> = _errorMessage

    init {
        widgetsUseCase.onProductLoadedListener = { _productData.postValue(it) }
        widgetsUseCase.onErrorOccurredListener = { _errorMessage.postValue(it) }
    }

    fun loadWidgets(needUpdate: Boolean = false) {
        if (connectivityUtil.isOnline.not()) {
            _noInternet.postValue(true)
            return
        }
        if (_productData.value == null || needUpdate)
            widgetsUseCase.loadWidgets()
    }
}
