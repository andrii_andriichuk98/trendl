package com.example.trendl.presentation.dialogs

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import androidx.annotation.StringDef
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import com.example.trendl.R
import com.example.trendl.databinding.DialogOneButtonBinding
import com.example.trendl.presentation.base.BaseDialog
import com.example.trendl.presentation.utils.finishWithResult
import kotlinx.parcelize.Parcelize

class OneButtonDialog : BaseDialog<DialogOneButtonBinding>() {

    override fun getLayoutResId(): Int = R.layout.dialog_one_button

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        navController = findNavController()
        val view = View.inflate(requireContext(), getLayoutResId(), null)
        return Dialog(requireActivity(), android.R.style.Theme_Material_Dialog).apply {
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(view)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val dialogType = arguments?.getString(DIALOG_TYPE).orEmpty()
        initUI(dialogType)
        initListeners()
    }

    private fun initUI(dialogType: String) {
        when (dialogType) {
            TYPE_NO_INTERNET_CONNECTION -> {
                setupDialogText(
                    titleText = getString(R.string.no_internet_connection),
                    messageText = getString(R.string.please_check_internet_connection),
                    buttonText = getString(R.string.try_again),
                )
            }
            else -> dismiss()
        }
    }

    private fun setupDialogText(titleText: String, messageText: String, buttonText: String) =
        with(binding) {
            tvTitle.isVisible = titleText.isNotBlank()
            tvTitle.text = titleText
            tvMessage.text = messageText
            btnPositive.text = buttonText
        }

    private fun initListeners() {
        binding.btnPositive.setOnClickListener {
            navController.finishWithResult(OneButtonDialogResult())
        }
    }

    companion object {

        @Retention(AnnotationRetention.SOURCE)
        @StringDef(
            TYPE_NO_INTERNET_CONNECTION,
        )
        annotation class DialogType

        const val TYPE_NO_INTERNET_CONNECTION = "no_internet_connection"

        private const val DIALOG_TYPE = "dialog_type"

        fun buildArgs(@DialogType dialogType: String): Bundle = bundleOf(
            DIALOG_TYPE to dialogType,
        )
    }
}

@Parcelize
class OneButtonDialogResult : Parcelable