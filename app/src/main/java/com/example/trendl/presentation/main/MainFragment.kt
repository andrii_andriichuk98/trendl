package com.example.trendl.presentation.main

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.hilt.navigation.fragment.hiltNavGraphViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.trendl.R
import com.example.trendl.data.uimodel.BaseWidgetUIModel
import com.example.trendl.data.uimodel.DisplayUiModel
import com.example.trendl.data.uimodel.ProductUIModel
import com.example.trendl.databinding.MainFragmentBinding
import com.example.trendl.presentation.base.BaseFragment
import com.example.trendl.presentation.dialogs.OneButtonDialog
import com.example.trendl.presentation.dialogs.OneButtonDialogResult
import com.example.trendl.presentation.main.adapter.createPicturesAdapter
import com.example.trendl.presentation.product_details.ProductDetailsFragment
import com.example.trendl.presentation.utils.handleResult
import com.example.trendl.presentation.utils.route
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : BaseFragment<MainFragmentBinding>() {
    private val viewModel: MainViewModel by hiltNavGraphViewModels(R.id.mainFragment)

    private val widgetsAdapter: ListDelegationAdapter<List<BaseWidgetUIModel>> by lazy(
        LazyThreadSafetyMode.NONE
    ) {
        createPicturesAdapter(::onItemSelected)
    }

    override fun getLayoutResId(): Int = R.layout.main_fragment

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initRecyclerView()
        initObservers()
        initSwipeRefresh()

        viewModel.loadWidgets()
    }

    private fun initRecyclerView() = with(binding) {
        rvWidgets.layoutManager =
            LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        rvWidgets.adapter = widgetsAdapter
    }

    private fun initObservers() {
        viewModel.productData.observe(viewLifecycleOwner) { trendModel ->
            widgetsAdapter.items = trendModel.sortedBy { it.displayOrder }
            binding.rvWidgets.adapter = widgetsAdapter
            binding.swipeRefresh.isRefreshing = false
        }
        viewModel.errorMessage.observe(viewLifecycleOwner) {
            Toast.makeText(requireContext(), it.toString(), Toast.LENGTH_SHORT).show()
        }
        viewModel.noInternet.observe(viewLifecycleOwner) {
            displayNoInternetDialog()
        }
        navController.handleResult<OneButtonDialogResult>(
            viewLifecycleOwner,
            R.id.mainFragment,
            R.id.oneButtonDialog
        ) {
            viewModel.loadWidgets(needUpdate = true)
        }
    }

    private fun initSwipeRefresh() {
        binding.swipeRefresh.setOnRefreshListener {
            viewModel.loadWidgets(needUpdate = true)
        }
    }

    private fun onItemSelected(product: DisplayUiModel) {
        if (product is ProductUIModel) {
            navigateToProductDetails(product)
        }
    }

    private fun displayNoInternetDialog() {
        val args = OneButtonDialog.buildArgs(OneButtonDialog.TYPE_NO_INTERNET_CONNECTION)
        navController.route(R.id.oneButtonDialog, args)
    }

    private fun navigateToProductDetails(product: ProductUIModel) {
        val args = ProductDetailsFragment.buildArgs(product)
        navController.route(
            R.id.productDetailsFragment,
            args,
            popUpToFragment = R.id.mainFragment
        )
    }

    companion object {
        const val POSITION_SECOND = 1
    }
}