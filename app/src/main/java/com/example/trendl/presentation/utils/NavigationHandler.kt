package com.example.trendl.presentation.utils

import android.os.Bundle
import android.os.Parcelable
import androidx.annotation.AnimRes
import androidx.annotation.IdRes
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.example.trendl.R

/**
 * @param popUpToFragment - set to return to this fragment after back press on opened screen
 */
fun NavController.route(
    @IdRes directionId: Int,
    args: Bundle = Bundle(),
    @IdRes popUpToFragment: Int? = null,
    launchSingleTop: Boolean = true,
    @AnimRes enterAnim: Int = R.anim.fragment_fade_in,
    @AnimRes exitAnim: Int = R.anim.fragment_fade_out,
    @AnimRes popEnterAnim: Int = R.anim.fragment_fade_in,
    @AnimRes popExitAnim: Int = R.anim.fragment_fade_out
) {
    val navigationOptions = buildNavigationOptions(
        popUpToFragment = popUpToFragment,
        launchSingleTop = launchSingleTop,
        enterAnim = enterAnim,
        exitAnim = exitAnim,
        popEnterAnim = popEnterAnim,
        popExitAnim = popExitAnim
    )
    navigate(directionId, args, navigationOptions)
}

private fun buildNavigationOptions(
    @IdRes popUpToFragment: Int?,
    launchSingleTop: Boolean,
    @AnimRes enterAnim: Int,
    @AnimRes exitAnim: Int,
    @AnimRes popEnterAnim: Int,
    @AnimRes popExitAnim: Int
): NavOptions {
    val optionBuilder = NavOptions.Builder()
        .setEnterAnim(enterAnim)
        .setExitAnim(exitAnim)
        .setPopEnterAnim(popEnterAnim)
        .setPopExitAnim(popExitAnim)
        .setLaunchSingleTop(launchSingleTop)
    popUpToFragment?.let { popUpId ->
        optionBuilder.setPopUpTo(popUpId, false)
    }
    return optionBuilder.build()
}

fun <T : Parcelable> NavController.finishWithResult(result: T) {
    val currentDestinationId = currentDestination?.id
    if (currentDestinationId != null) {
        previousBackStackEntry?.savedStateHandle?.set(resultName(currentDestinationId), result)
    }
    popBackStack()
}


// https://developer.android.com/guide/navigation/navigation-programmatic
fun <T : Parcelable> NavController.handleResult(
    lifecycleOwner: LifecycleOwner,
    @IdRes currentDestinationId: Int,
    @IdRes childDestinationId: Int,
    handler: (T) -> Unit
) {
    // `getCurrentBackStackEntry` doesn't work in case of recovery from the process death when dialog is opened.
    val currentEntry = getBackStackEntry(currentDestinationId)
    val observer = LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_RESUME) {
            handleResultFromChild(childDestinationId, currentEntry, handler)
        }
    }
    currentEntry.lifecycle.addObserver(observer)

    // As addObserver() does not automatically remove the observer, we
    // call removeObserver() manually when the view lifecycle is destroyed
    lifecycleOwner.lifecycle.addObserver(
        LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_DESTROY) {
                currentEntry.lifecycle.removeObserver(observer)
            }
        }
    )
}

private fun <T : Parcelable> handleResultFromChild(
    @IdRes childDestinationId: Int,
    currentEntry: NavBackStackEntry,
    handler: (T) -> Unit
) {
    val expectedResultKey = resultName(childDestinationId)
    if (currentEntry.savedStateHandle.contains(expectedResultKey)) {
        val result = currentEntry.savedStateHandle.get<T>(expectedResultKey)
        handler(result!!)
        currentEntry.savedStateHandle.remove<T>(expectedResultKey)
    }
}

private fun resultName(resultSourceId: Int) = "result-$resultSourceId"