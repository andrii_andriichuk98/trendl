package com.example.trendl.presentation.utils.connectivity

import androidx.lifecycle.LifecycleOwner

interface IBaseConnectivityManager {
    fun setConnectivityListener(
        lifecycleOwner: LifecycleOwner?,
        connectivityListener: IConnectivityListener?
    )
}