package com.example.trendl.domain

import android.annotation.SuppressLint
import com.example.trendl.client.model.Content
import com.example.trendl.client.model.ProductListing
import com.example.trendl.client.model.Widgets
import com.example.trendl.data.repository.TrendRepository
import com.example.trendl.data.uimodel.*
import com.example.trendl.presentation.utils.splitUrl
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import javax.inject.Inject

class WidgetsUseCase @Inject constructor(
    private val trendRepository: TrendRepository,
) {
    var onProductLoadedListener: ((MutableList<BaseWidgetUIModel>?) -> Unit)? = null
    var onErrorOccurredListener: ((String?) -> Unit)? = null

    @SuppressLint("CheckResult")
    fun loadWidgets() {
        trendRepository
            .fetchData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ result ->
                Timber.d("Widgets : $result")
                loadProducts(result.widgets)
            }, { error ->
                Timber.d("Widgets ERROR: $error")
                onErrorOccurredListener?.invoke(error.message)
            })
    }

    @SuppressLint("CheckResult")
    private fun loadProducts(
        widgets: List<Widgets>
    ) {
        val filteredWidgets = widgets.filter { it.bannerContents.isNotEmpty() }

        val fullUrl = widgets
            .firstOrNull { it.fullServiceUrl.isNullOrEmpty().not() }
            ?.fullServiceUrl ?: ""

        val fullServiceUrlWithPage = widgets
            .firstOrNull { it.fullServiceUrlWithPage.isNullOrEmpty().not() }
            ?.fullServiceUrlWithPage ?: ""

        Observable.merge(
            Observable.fromIterable(filteredWidgets),
            trendRepository.loadProductsSlider(fullUrl.splitUrl()),
            trendRepository.loadProductsListing(fullServiceUrlWithPage.splitUrl())
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .map { emitter: Any ->
                return@map when {
                    emitter is ProductListing -> {
                        emitter.content.toProductUIModel() to DisplayType.PRODUCT_LISTING
                    }
                    (emitter is List<*> && emitter.firstOrNull() is Content) -> {
                        (emitter as List<Content>).toProductUIModel() to DisplayType.PRODUCT_SLIDER
                    }
                    (emitter is Widgets) -> {
                        emitter.bannerContents.toProductUIModel1() to
                                WidgetFactory.getDisplayType(emitter.displayType, emitter.type)
                    }
                    else -> {
                        emptyList<DisplayUiModel>() to DisplayType.UNKNOWN
                    }
                }
            }
            .map { content: Pair<List<DisplayUiModel>, DisplayType> ->
                WidgetFactory.createWidget(content.first, content.second)
            }
            .toList()
            .subscribe({ result ->
                Timber.d("Products & Banners : $result")
                onProductLoadedListener?.invoke((result) as? MutableList<BaseWidgetUIModel>)
            }, { error ->
                Timber.d("Products & Banners ERROR: $error")
                onErrorOccurredListener?.invoke(error.message)
            })
    }
}