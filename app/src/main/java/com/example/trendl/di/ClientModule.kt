package com.example.trendl.di

import android.content.Context
import com.example.trendl.data.repository.TrendRepository
import com.example.trendl.data.repository.TrendRepositoryImpl
import com.example.trendl.presentation.utils.connectivity.ConnectivityUtil
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext

@Module
@InstallIn(ViewModelComponent::class)
object ClientModule {
    @Provides
    fun bindPermissionDispatcher(
    ): TrendRepository {
        return TrendRepositoryImpl()
    }

    @Provides
    fun provideConnectivityManager(@ApplicationContext context: Context): ConnectivityUtil {
        return ConnectivityUtil(context)
    }
}