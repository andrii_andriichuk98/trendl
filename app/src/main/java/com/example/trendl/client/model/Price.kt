package com.example.trendl.client.model

data class Price (

	val salePrice : Double? = null,
	val marketPrice : Double? = null,
	val newDiscountedPrice : Double? = null,
	val mOriginalPrice : Int? = null,
	val discountedPriceInfo : String? = null
)