package com.example.trendl.client.model

data class BusinessUnitData(
    val id: Int? = null,
    val name: String? = null,
    val isSexualContent: Boolean? = null,
    val isDigitalGoods: Boolean? = null
)