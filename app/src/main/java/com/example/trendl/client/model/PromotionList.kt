package com.example.trendl.client.model

data class PromotionList (

	val id : String? = null,
	val name : String? = null,
	val shortname : String? = null,
	val type : String? = null,
	val icon : String? = null,
	val colorCode : String? = null,
	val endsIn24Hours : Boolean? = null
)