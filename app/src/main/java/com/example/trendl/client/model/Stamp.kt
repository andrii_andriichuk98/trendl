package com.example.trendl.client.model

data class Stamp (

	val imageUrl : String? = null,
	val position : String? = null,
	val aspectRatio : Double? = null,
	val type : String? = null
)