package com.example.trendl.client.model

data class ProductListing (
	val totalElements : Int,
	val totalPages : Int,
	val page : Int,
	val size : Int,
	val content : List<Content>
)