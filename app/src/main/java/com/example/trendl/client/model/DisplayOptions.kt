package com.example.trendl.client.model

data class DisplayOptions(
    val showProductPrice: Boolean,
    val showProductFavoredButton: Boolean,
    val showClearButton: Boolean,
    val showCountdown: Boolean,
    val showCountOnTitle: Boolean
)