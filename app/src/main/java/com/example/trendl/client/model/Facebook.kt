package com.example.trendl.client.model

data class Facebook (

	val id : String? = null,
	val item_price : Double? = null,
	val product_boutiqueid : String? = null,
	val product_contentid : String? = null,
	val product_itemnumber : String? = null,
	val product_listingid : String? = null,
	val product_merchantid : String? = null,
	val quantity : String? = null
)