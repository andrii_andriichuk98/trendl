package com.example.trendl.client.model

data class TrendModel(
    val widgets: List<Widgets>,
    val pagination: Pagination
)