package com.example.trendl.client.model

data class BannerContents(
    val bannerEventKey: String,
    val displayOrder: Int,
    val imageUrl: String,
    val title: String,
    val marketing: Marketing
)