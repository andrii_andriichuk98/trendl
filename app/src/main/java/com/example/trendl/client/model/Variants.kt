package com.example.trendl.client.model

data class Variants (

	val campaignId : Int? = null,
	val listingId : String? = null,
	val name : String? = null,
	val variantId : Int? = null,
	val value : String? = null,
	val price : Price? = null
)