package com.example.trendl.client.model

data class Marketing (
	val adjust : String? = null,
	val campaignsAnalytics : String? = null,
	val criteo : String? = null,
	val enhanced : Enhanced? = null,
	val facebook : Facebook? = null,
	val delphoi : Delphoi? = null
)