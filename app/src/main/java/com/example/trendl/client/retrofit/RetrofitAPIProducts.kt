package com.example.trendl.client.retrofit

import com.example.trendl.client.model.ProductListing
import com.example.trendl.client.model.ProductSliderModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface RetrofitAPIProducts {
    @GET
    fun loadProductsSlider(@Url url: String): Observable<ProductSliderModel>

    @GET
    fun loadProductsListing(@Url url: String): Observable<ProductListing>
}