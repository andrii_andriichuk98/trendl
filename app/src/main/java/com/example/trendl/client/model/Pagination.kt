package com.example.trendl.client.model

data class Pagination(
    val currentPage: Int,
    val pageSize: Int,
    val totalCount: Int
)