package com.example.trendl.client.model

data class Enhanced(

    val dimension140: String? = null,
    val dimension141: String? = null,
    val dimension142: String? = null,
    val dimension145: String? = null,
    val dimension146: String? = null,
    val dimension147: String? = null,
    val dimension149: String? = null,
    val dimension152: String? = null,
    val dimension155: String? = null,
    val dimension156: String? = null,
    val dimension191: String? = null
)