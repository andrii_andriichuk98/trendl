package com.example.trendl.client.retrofit

import com.example.trendl.client.model.TrendModel
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface RetrofitAPI {
    @Headers("Content-Type: application/json", "Build: 512")
    @GET("widget/display/personalized/")
    fun fetchData(
        @Query("widgetPageName") widgetPageName: String = "interview",
        @Query("platform") platform: String = "android"
    ): Observable<TrendModel>
}