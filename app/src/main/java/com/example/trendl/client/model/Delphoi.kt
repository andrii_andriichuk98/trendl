package com.example.trendl.client.model

data class Delphoi (
	val tv040 : String? = null,
	val tv067 : String? = null,
	val tv070 : String? = null,
	val tv072 : String? = null,
	val tv073 : String? = null,
	val tv076 : String? = null,
	val tv097 : String? = null
)