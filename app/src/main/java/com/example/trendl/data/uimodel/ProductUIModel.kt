package com.example.trendl.data.uimodel

import android.os.Parcelable
import com.example.trendl.client.model.BannerContents
import com.example.trendl.client.model.Content
import kotlinx.parcelize.Parcelize

@Parcelize
open class DisplayUiModel(
    open val imageUrl: String?,
    open val name: String?
) : Parcelable

data class ProductUIModel(
    override val imageUrl: String? = null,
    override val name: String? = null,
    val id: Int? = null,
    val brandName: String? = null,
    val businessUnit: String? = null,
    val categoryName: String? = null,
    val imageUrls: List<String>? = null,
    val marketPrice: Double? = null,
    val promotions: List<String>? = null,
    val salePrice: Double? = null,
    val mOriginalPrice: Double? = null,
    val discountedPriceInfo: String? = null,
    val promotionMessage: String? = null,
    val averageRating: Float? = null,
    val discountedPrice: Double? = null,
    val newDiscountedPrice: Double? = null
) : DisplayUiModel(imageUrl, name) {
    constructor(content: Content) : this(
        content.imageUrl,
        content.name,
        content.id,
        content.brandName,
        content.businessUnit,
        content.categoryName,
        content.imageUrls,
        content.marketPrice,
        content.promotions,
        content.salePrice,
        content.mOriginalPrice,
        content.discountedPriceInfo,
        content.promotionMessage,
        content.averageRating?.toFloat(),
        content.discountedPrice,
        content.newDiscountedPrice
    )
}

data class BannerUiModel(
    override val imageUrl: String?,
    override val name: String?
) : DisplayUiModel(imageUrl, name) {
    constructor(bannerContents: BannerContents) : this(
        bannerContents.imageUrl,
        bannerContents.title
    )
}

