package com.example.trendl.data.repository

import com.example.trendl.client.model.ProductListing
import com.example.trendl.client.model.ProductSliderModel
import com.example.trendl.client.model.TrendModel
import com.example.trendl.client.retrofit.RetrofitAPI
import com.example.trendl.client.retrofit.RetrofitInstance
import com.example.trendl.client.retrofit.RetrofitAPIProducts
import io.reactivex.Observable

class TrendRepositoryImpl : TrendRepository {
    override fun fetchData(): Observable<TrendModel> {
        return RetrofitInstance.getRetrofit().create(RetrofitAPI::class.java)
            .fetchData()
    }

    override fun loadProductsSlider(urlPair: Pair<String, String>): Observable<ProductSliderModel> {
        return RetrofitInstance.getRetrofitForProducts(urlPair.first)
            .create(RetrofitAPIProducts::class.java)
            .loadProductsSlider(urlPair.second)
    }

    override fun loadProductsListing(urlPair: Pair<String, String>): Observable<ProductListing> {
        return RetrofitInstance.getRetrofitForProducts(urlPair.first)
            .create(RetrofitAPIProducts::class.java)
            .loadProductsListing(urlPair.second)
    }
}