package com.example.trendl.data.repository

import com.example.trendl.client.model.ProductListing
import com.example.trendl.client.model.ProductSliderModel
import com.example.trendl.client.model.TrendModel
import io.reactivex.Observable

interface TrendRepository {
    fun fetchData(): Observable<TrendModel>
    fun loadProductsSlider(urlPair: Pair<String, String>): Observable<ProductSliderModel>
    fun loadProductsListing(urlPair: Pair<String, String>): Observable<ProductListing>
}