package com.example.trendl.data.uimodel

import com.example.trendl.client.model.BannerContents
import com.example.trendl.client.model.Content

/*------------------------------------------------------------------------------------------------*/
open class BaseWidgetUIModel(
    open val contents: List<DisplayUiModel>,
    open val displayOrder: Int = 0
)

data class SingleBannerUIModel(
    override val contents: List<DisplayUiModel>,
    override val displayOrder: Int = 1
) : BaseWidgetUIModel(contents, displayOrder)

data class CarouselBannerUIModel(
    override val contents: List<DisplayUiModel>,
    override val displayOrder: Int = 5
) : BaseWidgetUIModel(contents, displayOrder)

data class SliderBannerUIModel(
    override val contents: List<DisplayUiModel>,
    override val displayOrder: Int = 3
) : BaseWidgetUIModel(contents, displayOrder)

data class ProductListUIModel(
    override val contents: List<DisplayUiModel>,
    override val displayOrder: Int = 4
) : BaseWidgetUIModel(contents, displayOrder)

data class ProductSliderUIModel(
    override val contents: List<DisplayUiModel>,
    override val displayOrder: Int = 2
) : BaseWidgetUIModel(contents, displayOrder)

enum class DisplayType {
    SINGLE_BANNER,
    PRODUCT_SLIDER,
    BANNER_SLIDER,
    PRODUCT_LISTING,
    CAROUSEL_BANNER,
    UNKNOWN
}

fun List<Content>?.toProductUIModel() =
    this?.map {
        ProductUIModel(it)
    } ?: emptyList()

fun List<BannerContents>?.toProductUIModel1() =
    this?.map {
        BannerUiModel(it)
    } ?: emptyList()

object WidgetFactory {
    fun createWidget(content: List<DisplayUiModel>, displayType: DisplayType): BaseWidgetUIModel {
        return when (displayType) {
            DisplayType.SINGLE_BANNER -> SingleBannerUIModel(content)
            DisplayType.PRODUCT_SLIDER -> ProductSliderUIModel(content)
            DisplayType.BANNER_SLIDER -> SliderBannerUIModel(content)
            DisplayType.PRODUCT_LISTING -> ProductListUIModel(content)
            DisplayType.CAROUSEL_BANNER -> CarouselBannerUIModel(content)
            DisplayType.UNKNOWN -> BaseWidgetUIModel(emptyList())
        }
    }

    fun getDisplayType(displayType: String, type: String): DisplayType {
        return when {
            displayType == "CAROUSEL" && type == "BANNER" -> DisplayType.CAROUSEL_BANNER
            displayType == "SLIDER" && type == "BANNER" -> DisplayType.BANNER_SLIDER
            displayType == "SINGLE" && type == "BANNER" -> DisplayType.SINGLE_BANNER
            else -> DisplayType.PRODUCT_SLIDER
        }
    }

}