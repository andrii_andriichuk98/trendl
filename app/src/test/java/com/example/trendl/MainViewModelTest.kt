package com.example.trendl

import android.content.Context
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.trendl.data.repository.TrendRepository
import com.example.trendl.data.repository.TrendRepositoryImpl
import com.example.trendl.domain.WidgetsUseCase
import com.example.trendl.presentation.main.MainViewModel
import com.example.trendl.presentation.utils.connectivity.ConnectivityUtil
import junit.framework.TestCase
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainViewModelTest : TestCase() {
    private lateinit var mainViewModel: MainViewModel

    public override fun setUp() {
        super.setUp()
        val context = ApplicationProvider.getApplicationContext<Context>()
        val connectivityUtil = ConnectivityUtil(context)
        val trendRepository: TrendRepository = TrendRepositoryImpl()
        val widgetsUseCase = WidgetsUseCase(trendRepository)
        mainViewModel = MainViewModel(widgetsUseCase, connectivityUtil)
    }

    @Test(timeout = 500)
    fun testViewModel() {
        val widgetsData = mainViewModel.loadWidgets(needUpdate = true)
        assertNotNull(widgetsData)
    }
}